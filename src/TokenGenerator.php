<?php

namespace Drupal\protected_routes;

use Drupal\Component\Utility\Random;
use Drupal\Core\Site\Settings;
use Drupal\protected_routes\Event\ProtectedRouteEvent;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class TokenGenerator.
 */
class TokenGenerator {

  const DEFAULT_ALG = 'HS256';

  protected $key;

  protected $dispatcher;

  /**
   * Constructs a new TokenGenerator object.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   Settings Object.
   */
  public function __construct(Settings $settings, EventDispatcherInterface $dispatcher) {
    $this->key = $settings::getHashSalt();
    $this->dispatcher = $dispatcher;
  }

  /**
   * Generates a token.
   *
   * @param array $data
   *   Array of data.
   * @param bool $expire
   *   Expiration flag.
   * @param string $alg
   *   The jwt algorithm.
   *
   * @return string
   *   The JWT token.
   *
   * @throws \Exception
   */
  public function get(array $data = [], $expire = TRUE, $alg = self::DEFAULT_ALG) {
    if (empty($data)) {
      $data = $this->generateRandomData();
    }
    if ($expire) {
      $data['expire'] = $this->calculateExpirationDate();
    }

    $event = new ProtectedRouteEvent($data);
    $this->dispatcher->dispatch(ProtectedRouteEvent::NAME, $event);

    return JWT::encode($event->getData(), $this->key, $alg);
  }

  /**
   * Generates radom data.
   *
   * @return array
   *   A random key value array.
   */
  protected function generateRandomData() {
    return ['key_' . (new Random())->string(6) => 'value_' . (new Random())->string(50)];
  }

  /**
   * Validates a JWT hash.
   *
   * @param string $jwt
   *   The JWT token to decode.
   * @param array $allowed_algs
   *   The allowed algorithms used to decode the JWT token.
   *
   * @return array|bool
   *   The result of the validation
   *
   * @throws \Exception
   */
  public function validate(string $jwt, array $allowed_algs = [self::DEFAULT_ALG]) {

    try {
      $result = (array) JWT::decode($jwt, $this->key, $allowed_algs);
    }
    catch (\InvalidArgumentException | \UnexpectedValueException | BeforeValidException | ExpiredException | SignatureInvalidException $e) {
      $result = FALSE;
    }

    return $result;
  }

  /**
   * Calculate the expiration date.
   *
   * @return int
   *   Expiration Timestamp.
   *
   * @throws \Exception
   */
  public function calculateExpirationDate() {
    // @todo: Get from config.
    $expire = new \DateTime('today +2 days');
    return $expire->getTimestamp();
  }

}
