<?php
namespace Drupal\Tests\protected_routes\Unit;

use Drupal\Core\Site\Settings;
use Drupal\protected_routes\TokenGenerator;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class TokenGeneratorTest.
 */
class TokenGeneratorTest extends UnitTestCase {

  /**
   * The Token generator.
   *
   * @var \Drupal\protected_routes\TokenGenerator
   */
  protected $tokenGenerator;

  /**
   * {@inheritDoc}
   */
  protected function setUp() {
    parent::setUp();
    $settings = [
      'hash_salt' => 'hash-salt-key',
    ];

    $this->tokenGenerator = new TokenGenerator(new Settings($settings), new EventDispatcher());
  }

  /**
   * Tests if token is correctly generated and validated.
   */
  public function testGenerateAndValidateToken() {

    $jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIn0.FJTPzhSh7lXmEiU6IpWs1j5s7QWrkR2KH2L2M8VmQJg';
    $tokenGenerator = $this->tokenGenerator;
    $payload = ["sub" => "1234567890"];

    $this->assertEquals($tokenGenerator->get($payload, FALSE), $jwt);
    $this->assertEquals($tokenGenerator->validate($jwt), $payload);
  }

  /**
   * Tests an invalid token.
   */
  public function testInvalidToken() {

    $jwt = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIn0.FJTPzhSh7lXmEiU6IpWs1j5s7QWrkR2KH2L2M8VmQJgInvalid';
    $tokenGenerator = $this->tokenGenerator;

    $this->assertFalse($tokenGenerator->validate($jwt));
  }

  /**
   * Tests if a token has expire data.
   */
  public function testTokenHasExpiration() {
    $tokenGenerator = $this->tokenGenerator;
    $token = $tokenGenerator->get(["sub" => "1234567890"]);
    $this->assertArrayHasKey('expire', $tokenGenerator->validate($token));
  }

  /**
   * Tests if a token doesn't have expire data.
   */
  public function testTokenHasNoExpiration() {
    $tokenGenerator = $this->tokenGenerator;
    $token = $tokenGenerator->get(["sub" => "1234567890"], FALSE);
    $this->assertArrayNotHasKey('expire', $tokenGenerator->validate($token));
  }

  /**
   * Tests expired token.
   */
  public function testTokenExpired() {
    $tokenGenerator = $this->tokenGenerator;
    // We manually force expire date.
    $token = $tokenGenerator->get(["sub" => "1234567890", 'expire' => '946684800'], FALSE);
    $this->assertFalse($tokenGenerator->validate($token));
  }

}
