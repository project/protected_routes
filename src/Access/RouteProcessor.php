<?php

namespace Drupal\protected_routes\Access;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\RouteProcessor\OutboundRouteProcessorInterface;
use Drupal\protected_routes\TokenGenerator;
use Symfony\Component\Routing\Route;

/**
 * Class RouteProcessor.
 */
class RouteProcessor implements OutboundRouteProcessorInterface {

  /**
   * @var \Drupal\protected_routes\TokenGenerator
   */
  protected $tockenGenerator;

  /**
   * RouteProcessor constructor.
   *
   * @param \Drupal\protected_routes\TokenGenerator $generator
   *   The token generator.
   */
  public function __construct(TokenGenerator $generator) {
    $this->tockenGenerator = $generator;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($route_name, Route $route, array &$parameters, BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($route->hasRequirement('_protected')) {
      $path = ltrim($route->getPath(), '/');
      foreach ($parameters as $param => $value) {
        $path = str_replace("{{$param}}", $value, $path);
      }

      if (!$bubbleable_metadata) {
        $parameters['_token'] = $this->tockenGenerator->get(['path' => $path]);
      }
      else {
        $placeholder = $this->tockenGenerator->get(['path' => $path]);
        $placeholder_render_array = [
          '#lazy_builder' => ['protected_routes.route_processor:renderPlaceholderToken', [$path]],
        ];

        $parameters['_token'] = $placeholder;
        $bubbleable_metadata->addAttachments(['placeholders' => [$placeholder => $placeholder_render_array]]);
      }
    }
  }

  /**
   * Lazy builer #lazy_builder callback; gets a token for the given path.
   *
   * @param string $path
   *   The path to get a CSRF token for.
   *
   * @return array
   *   A renderable array representing the CSRF token.
   *
   * @see: RouteProcessorCsrf::renderPlaceholderCsrfToken
   */
  public function renderPlaceholderToken($path) {
    $data = [
      'path' => $path,
    ];

    return [
      '#markup' => $this->tockenGenerator->get($data),
    ];
  }

}