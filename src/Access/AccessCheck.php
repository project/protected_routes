<?php

namespace Drupal\protected_routes\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface as RoutingAccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\protected_routes\TokenGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Class AccessCheck.
 */
class AccessCheck implements RoutingAccessInterface {

  /**
   * The Token generator.
   *
   * @var \Drupal\protected_routes\TokenGenerator
   */
  protected $tokenGenerator;

  /**
   * AccessCheck constructor.
   *
   * @param \Drupal\protected_routes\TokenGenerator $generator
   *   The token generator.
   */
  public function __construct(TokenGenerator $generator) {
    $this->tokenGenerator = $generator;
  }

  /**
   * {@inheritDoc}
   */
  public function access(Route $route, Request $request, RouteMatchInterface $route_match) {
    $parameters = $route_match->getRawParameters();
    $path = ltrim($route->getPath(), '/');
    // Replace the path parameters with values from the parameters array.
    foreach ($parameters as $param => $value) {
      $path = str_replace("{{$param}}", $value, $path);
    }

    if ($data = $this->tokenGenerator->validate($request->query->get('_token', ''))) {
      $allowed = ($data['path'] === $path) ? TRUE : FALSE;
    }
    else {
      $allowed = FALSE;
    }

    $result = ($allowed === TRUE) ? AccessResult::allowed() : AccessResult::forbidden($request->query->has('_token') ? "'_token' URL query argument is invalid." : "'_token' URL query argument is missing.");
    // Not cacheable because the CSRF token is highly dynamic.
    return $result->setCacheMaxAge(0);
  }

}
