<?php

namespace Drupal\protected_routes\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class ProtectedRouteEvent.
 */
class ProtectedRouteEvent extends Event {
  const NAME = 'token.data_added';

  /**
   * The data.
   *
   * @var array
   */
  protected $data;

  /**
   * ProtectedRouteEvent constructor.
   *
   * @param array $data
   *   The data.
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * Get the data from the event.
   *
   * @return array
   *   The data.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Set the data.
   *
   * @param array $data
   *   The data to set.
   */
  public function setData(array $data) {
    $this->data = $data;
  }

}
