<?php

namespace Drupal\protected_routes\EventSubscriber;

use Drupal\protected_routes\Event\ProtectedRouteEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ProtectedRouteSubscriber.
 */
abstract class ProtectedRouteSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ProtectedRouteEvent::NAME][] = ['onDataAdd', 800];
    return $events;
  }

  /**
   * Method called before generating the token.
   *
   * This allows to alter the data passed to the token before it is hashed.
   *
   * @param \Drupal\protected_routes\Event\ProtectedRouteEvent $event
   *   The event.
   */
  public function onDataAdd(ProtectedRouteEvent $event) {

  }

}
